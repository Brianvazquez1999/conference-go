from datetime import datetime
import json
import pika
from pika.exceptions import AMQPConnectionError
import django
import os
import sys
import time


sys.path.append("")
os.environ.setdefault("DJANGO_SETTINGS_MODULE", "attendees_bc.settings")
django.setup()

from attendees.models import AccountVo




def update_account(ch, method, properties, body):
    data = json.loads(body)
    first_name = data["first_name"]
    last_name = data["last_name"]
    email = data["email"]
    is_active = data["is_active"]
    print(is_active)
    updated_string = data["updated"]
    updated = datetime.fromisoformat(updated_string)
    if is_active:
        print("hereerrrreeeeeeee")
        AccountVo.objects.update_or_create(email=email, defaults={"first_name": first_name, "last_name": last_name, "is_active": is_active, "updated": updated, "email": email} )
    else:
        AccountVo.objects.get(email=email).delete()

while True:
    try:
        connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq"))
        channel = connection.channel()
        channel.exchange_declare(exchange="account_info", exchange_type='fanout')
        result = channel.queue_declare(queue='gddfgfd', exclusive=True)
        queue_name = result.method.queue
        channel.queue_bind(exchange="account_info", queue=queue_name)
        channel.basic_consume(
            queue=queue_name,
            on_message_callback=update_account,
            auto_ack=True,
        )
        channel.start_consuming()
    except AMQPConnectionError:
        print("cannot connect to RabbitMQ")
        time.sleep(2)












connection = pika.BlockingConnection(pika.ConnectionParameters("rabbitmq"))
channel = connection.channel()
channel.queue_declare(queue="presentation_approvals")
channel.queue_declare(queue="presentation_rejections")
channel.basic_consume(
    queue="presentation_approvals",
    on_message_callback=approve,
    auto_ack=True,
)
channel.basic_consume(
    queue="presentation_rejections",
    on_message_callback=reject,
    auto_ack=True,
)
channel.start_consuming()
