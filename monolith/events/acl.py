import requests
from .keys import PEXELS_API_KEY, OPENWEATHER_API_KEY

def location_url(city, state):
    headers = { "AUTHORIZATION": PEXELS_API_KEY}
    resp = requests.get(f"https://api.pexels.com/v1/search?query={city}+{state}", headers=headers)
    data = resp.json()["photos"][0]["url"]
    return data


def get_weather(city, state):
    params = {
        "q": f"{city}, {state}, US",
        "appid": OPENWEATHER_API_KEY
    }

    url = "http://api.openweathermap.org/geo/1.0/direct?"

    response = requests.get(url, params=params)
    content = response.json()
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except(KeyError, IndexError):
        return None

    url = "https://api.openweathermap.org/data/2.5/weather?"

    current_weather_params = {
        "lat":latitude,
        "lon": longitude,
        "appid": OPENWEATHER_API_KEY,
        "units": "imperial"
    }

    response = requests.get(url, params=current_weather_params)
    content = response.json()
    try:
        description = content["weather"][0]["description"]
        temp = content["main"]["temp"]
    except(KeyError, IndexError):
        return None

    return {"description": description, "temp": temp}
